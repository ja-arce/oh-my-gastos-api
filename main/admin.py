from django.contrib import admin
from main.models import Budget, Household, Member, Expense


class BudgetAdmin(admin.ModelAdmin):
    pass


class HouseholdAdmin(admin.ModelAdmin):
    pass


class MemberAdmin(admin.ModelAdmin):
    pass


class ExpenseAdmin(admin.ModelAdmin):
    pass


admin.site.register(Budget, BudgetAdmin)
admin.site.register(Household, HouseholdAdmin)
admin.site.register(Member, MemberAdmin)
admin.site.register(Expense, ExpenseAdmin)