from django.forms import widgets
from rest_framework import serializers
from django.contrib.auth.models import User
from provider.oauth2.models import AccessToken, Client
from main.models import *


class OwnerSerializer(serializers.ModelSerializer):

    user_details = serializers.SerializerMethodField()

    def get_user_details(self, obj):
        return {
            "username": obj.user.username,
            "email": obj.user.email,
        }

    class Meta:
        model = Member


class ConnectSocialMediaSerializer(serializers.ModelSerializer):
    """
    This serializer will accept:
    validated_data
     * email
     * username
     * avatar
     * social_id (to know what it is)
     * social_media (whether facebook or google)
     * first_name and last_name
    """

    def create(self, validated_data):
        social_id = ""
        # check if the user is already connected.
        the_avatar = validated_data["avatar"]
        if validated_data["social_media"] == "facebook":
            the_avatar = validated_data["avatar"] + "&oe=" + validated_data["oe"] + "&__gda__=" + validated_data["__gda__"]
        try:
            user = User.objects.get(email=validated_data["email"])
            user.first_name = validated_data["first_name"]
            user.last_name = validated_data["last_name"]
            user.save()
            try:
                owners = Member.objects.get(user=user)
                owners.avatar = the_avatar
                owners.save()
            except:
                owners = Member(
                    user=user,
                    avatar=the_avatar,
                )
                if validated_data["social_media"] == "facebook":
                    owners.facebook_id = validated_data["social_id"]
                owners.save()

            AccessToken.objects.create(user=user, client=Client.objects.filter()[0], scope=2)
            return owners
        except:
            pass
        # this is where the user is being created if he hasn't been connected yet.
        user = User(
            email=validated_data["email"],
            username=validated_data["email"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"]
        )
        user.set_unusable_password()
        user.save()
        owners = Member(
            user=user,
            avatar=the_avatar,
        )

        if validated_data["social_media"] == "facebook":
            owners.facebook_id = validated_data["social_id"]
        owners.save()
        AccessToken.objects.create(user=user, client=Client.objects.filter()[0], scope=2)
        
        return owners

    class Meta:
        model = Member
        exclude = ("user", "household", "facebook_id")
