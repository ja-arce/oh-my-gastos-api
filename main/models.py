from django.contrib.auth.models import User
from django.db import models


class Budget(models.Model):
    """
    This is a model class representation of a household budget.
    """
    years = [year for year in range(1960, 2015)]
    amount = models.PositiveIntegerField()
    date_created = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return unicode(self.amount)


class Household(models.Model):
    """
    This is a model class representation of a household.
    """
    name = models.CharField(max_length=500)
    budget = models.ManyToManyField(Budget)

    def __unicode__(self):
        return self.name


class Member(models.Model):
    """
    This is a model class representation of a member.
    """
    user = models.OneToOneField(User)
    household = models.ForeignKey(Household, null=True)
    is_head = models.BooleanField(default=False)
    avatar = models.CharField(max_length=500)
    facebook_id = models.CharField(max_length=500)

    def __unicode__(self):
        return self.user.username


class Expense(models.Model):
    """
    This is a model class representation of an expense per member.
    """
    date = models.DateTimeField(auto_now_add=True)
    amount = models.PositiveIntegerField()
    image = models.CharField(max_length=1000)
    member = models.ForeignKey(Member)
    name = models.CharField(max_length=500)

    def __unicode__(self):
        return self.member.household.name
