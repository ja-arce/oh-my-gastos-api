import json
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import authentication
from rest_framework import status
from rest_framework import permissions
from rest_framework.response import Response
from main.models import Member
from main.serializers import *
from rest_framework.decorators import *


@api_view(["POST"])
def connect_social_media(request):
    social_data = request.DATA
    is_first = False
    if social_data.get("social_media") == "facebook":
        try:
            member = Member.objects.get(facebook_id=social_data.get("social_id"))
        except:
            is_first = True
    serialized = ConnectSocialMediaSerializer(data=request.DATA)
    if serialized.is_valid():
        owner_data = serialized.create(request.DATA)
        access_token = AccessToken.objects.filter(user=owner_data.user).order_by("-expires")
        return Response({"access_token": access_token[0].token, "is_first": is_first}, status=status.HTTP_201_CREATED)
    return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)


def household_serializer(member):
    """
    :param household:
    :return: dictionary for json parsing
    """
    household = member.household
    expenses = Expense.objects.filter(member__household=member.household)
    budget = sum([budget.amount for budget in household.budget.all()])
    remaining = budget - sum([expense.amount for expense in expenses])

    level = 1
    if remaining > (budget/2):
        level = 1
    elif remaining > 0:
        level = 2
    else:
        level = 3

    household_dict = {
        "budget": budget,
        "expenses": sum([expense.amount for expense in expenses]),
        "remaining": remaining,
        "level": unicode(level),
        "household_name": household.name
    }

    return household_dict


def expense_serializer(expense):
    """

    :param expense:
    :return: dictionary for json parsing
    """

    expense_dict = {
        "amount": expense.amount,
        "image": expense.image,
        "name": expense.name,
        "creator": expense.member.user.username,
        "creator_image": expense.member.avatar,
    }

    return expense_dict



@api_view(["GET"])
@authentication_classes((authentication.OAuth2Authentication,))
@permission_classes((permissions.IsAuthenticated,))
def view_household_json(request):
    """
    :param request:
    :param pk:
    :return: a json formatted file of the household expenses and budget
    """
    member = get_object_or_404(Member, user=request.auth.user)
    return HttpResponse(json.dumps(household_serializer(member)), mimetype="application/json")


@api_view(["GET"])
@authentication_classes((authentication.OAuth2Authentication,))
@permission_classes((permissions.IsAuthenticated,))
def my_expenses(request):
    """
    :param request:
    :param pk:
    :return: a json formatted file of the household expenses and budget
    """
    member = get_object_or_404(Member, user=request.auth.user)
    # member = get_object_or_404(Member, pk=1)
    my_expenses = Expense.objects.filter(member__household=member.household).order_by("-date")

    expense_array = []
    for expense in my_expenses:
        expense_array.append(expense_serializer(expense))
    return HttpResponse(json.dumps(expense_array), mimetype="application/json")


@api_view(["POST"])
@authentication_classes((authentication.OAuth2Authentication,))
@permission_classes((permissions.IsAuthenticated,))
def create_expense(request):
    """
    :param request:
    :param pk:
    :return: a json formatted file of the household expenses and budget
    """
    member = get_object_or_404(Member, user=request.auth.user)
    expense = Expense.objects.create(amount=request.DATA.get("expense"), image=request.DATA.get("image"), name=request.DATA.get("name"), member=member)

    return HttpResponse(json.dumps(household_serializer(member)), mimetype="application/json")

