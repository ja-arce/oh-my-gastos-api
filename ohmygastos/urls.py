from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r"^household/expense/$", "main.views.create_expense"),
    url(r"^household/expenses/$", "main.views.my_expenses"),
    url(r"^household/$", "main.views.view_household_json"),
    url(r'^users/social-media/connect/$', 'main.views.connect_social_media'),
    url(r'^admin/', include(admin.site.urls)),
)
